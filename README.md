BotXO Formatter (Code Test)
===========================


Write a small app that takes an integer and return a string representation of that integer with commas separating
groups of 3 digits.

Run:
```bash
python3 formatter.py
```
