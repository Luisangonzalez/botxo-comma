#!/usr/bin/env python3

SPLITTER = ','
GROUP_SIZE = 3


def custom_group_format(number, splitter=SPLITTER, group_size=GROUP_SIZE):
    strNumber = str(number)

    result = []

    for i, char in enumerate(reversed(strNumber)):
        if i % group_size == 0 and i > 0:
            result.append(splitter)
        result.append(char)

    return "".join(reversed(result))


def get_input_number():
    while True:
        number = input("Insert number: ")
        try:
            return int(number)
        except ValueError:
            print("ERROR - Invalid number, please try again or tap Ctrl+C to exit...")


def main():
    number = get_input_number()
    custom_format = custom_group_format(number)

    # This method, I added it too because, in this case, it's the simple way for give it format,
    # but it isn't customizable
    thousands_format = "{:,d}".format(number)

    print("""\nRESULT:
    1º custom format: {}
    2º thousands format: {}
    """.format(custom_format, thousands_format))


if __name__ == '__main__':
    main()
